import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
//  eleUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

//  图标库
import 'font-awesome/css/font-awesome.min.css'

//  全局工具类
import { global } from '@/common/util/global'

Vue.config.productionTip = false

global(Vue)
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")
