import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'
// axios.defaults.withCredentials = true
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// 创建axios实例
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '/web/api' : '/web', // api的base_url
  timeout: 50000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  config.headers['xxs-client-type'] = 'web'
  return config
}, error => {
  console.log(error)
  Promise.reject(error)
})
export default service
