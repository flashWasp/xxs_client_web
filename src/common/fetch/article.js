import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'
// 创建axios实例
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '/xxs-news-server/api' : '/xxs-news-server', // api的base_url
  timeout: 50000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  config.headers['xxs-client-type'] = 'web'
  return config
}, error => {
  console.log(error)
  Promise.reject(error)
})
export default service
