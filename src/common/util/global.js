import articleFetch from '@/common/fetch/article' // 新闻模块请求封装
import webFetch from '@/common/fetch/web' // 新闻模块请求封装
import qs from 'qs'
const defaultSize = 10

export function global(Vue) {

  //  文章模块  ===========       start    ===========
  Vue.prototype.$articleFetchBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/xxs-news-server/api' : '/xxs-news-server'
  }
  Vue.prototype.$articleHttp = {
    get: function(url, params) {
      return articleFetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return articleFetch({
        url: url,
        method: 'post',
        // data: params
        data: params
      })
    }
  }
  //          ==========        end       ===========

  //  web总模块  ===========       start    ===========
  Vue.prototype.$webFetchBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/web/api' : '/web'
  }
  Vue.prototype.$webHttp = {
    get: function(url, params) {
      return webFetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return webFetch({
        url: url,
        method: 'post',
        data: params
        // data: qs.stringify(params)
      })
    }
  }
  //          ==========        end       ===========

  //  图片服务
  Vue.prototype.$imageBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/img-server/api' : '/img-server'
  }

  Vue.prototype.$mgImgUrl = 'http://111.230.209.144:8088/img-server/findSysAblum?imageNo='

  //  消息提示
  Vue.prototype.$tip = function(tip) {
    this.$message({
      message: tip,
      type: 'warning'
    });
  }
  //  成功的消息提示
  Vue.prototype.$msg = function(respones) {
    console.log(respones)
    if (respones.data.statusCode === 200) {
      this.$notify({
        title: '成功',
        message: respones.data.message,
        type: 'success'
      })
    } else {
      this.$notify.error({
        title: '错误',
        message: respones.data.message
      })
    }
  },
  //  判断是否有cookie
  Vue.prototype.$getCookie = function() {
    let flag = false
    if (document.cookie) {
      const cookies = document.cookie.split(';')
      cookies.forEach(item => {
        if (item.trim().substring(0, 11) === 'XxsWebToken') {
          flag = true
        }
      })
    }
    return flag
  }

  // 回到顶部
  Vue.prototype.$backToTop = function() {
    const start = window.pageYOffset
    let i = 0
    const backPosition = 0
    this.interval = setInterval(() => {
      let t = 10 * i
      let num = start / 2 * (--t * (t - 2) - 1) + start
      if ((t /= 500 / 2) < 1) {
        num = start / 2 * 10 * i * 10 * i - start
      }
      const next = Math.floor(num)
      if (next <= backPosition) {
        window.scrollTo(0, backPosition)
        clearInterval(this.interval)
      } else {
        window.scrollTo(0, next)
      }
      i++
    }, 16.7)
  }

  Vue.mixin({
    data() {
      return {
      }
    },
    methods: {
      $setPage(val) {
        if (window.localStorage) {
          const pageSizes = JSON.parse(window.localStorage.pageSizes || '{}') || {}
          pageSizes[this.$route.path] = val
          window.localStorage.pageSizes = JSON.stringify(pageSizes)
        }
      },
      $getPage(val) {
        if (window.localStorage) {
          const pageSizes = JSON.parse(window.localStorage.pageSizes || '{}') || {}
          return pageSizes[this.$route.path] || defaultSize
        }
        return defaultSize
      },
      $handleSizeChange(val) {
        this.$setPage(val)
        this.pageSize = val
        this.getTableList()
      },
      $handleCurrentChange(val) {
        this.currentPage = val
        this.getTableList()
      },
      $handleFilter() {
        this.currentPage = 1
        this.getTableList()
      }
    }
  })
}

