import Vue from "vue"
import Router from "vue-router"
import Home from "./views/Home.vue"

import techLayout from './views/technique/index.vue'

Vue.use(Router)

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/article",
      name: "article",
      component: () =>
        import("./views/article/article.vue")
    },
    {
      path: "/member",
      name: "member",
      component: () =>
        import("./views/user/userCenter.vue")
    },
    {
      path: "/search",
      name: "search",
      component: () =>
        import("./views/article/search.vue")
    },
    {
      path: "/company",
      name: "company",
      component: () =>
        import("./views/company/index.vue")
    },
    {
      path: "/companyDetail",
      name: "companyDetail",
      component: () =>
        import("./views/company/companyDetail.vue")
    },
    //  TODO
    // {
    //   path: "/techniqueTeam",
    //   name: "techniqueTeam",
    //   component: () =>
    //     import("./views/technique/index.vue")
    // },
    {
      path: "/techniqueTeam",
      component: techLayout,
      name: "技术团队",
      redirect: "/techniqueTeam/article",
      children: [
        { path: 'article', component: () => import('./views/technique/sub/techniqueArticle') },
        { path: 'techTalk', component: () => import('./views/technique/sub/techTalk') },
        { path: 'productShare', component: () => import('./views/technique/sub/productShare') },
        { path: 'abortWe', component: () => import('./views/technique/sub/abortWe') }
      ]
    }
  ]
});
